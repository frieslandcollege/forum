<?php
session_start();

if (isset($_SESSION['exception'])) {
    $e = $_SESSION['exception'];
    $json = json_decode($e, true);

} else {
    header("Location: ../");
    return;
}
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Exception: <?php echo $json['message'] ?></title>


    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
</head>


<body>
<link rel="stylesheet" href="https://jmblog.github.io/color-themes-for-google-code-prettify/themes/tomorrow-night.css">
<link rel="stylesheet" href="css/main.css">


<div class="col col-sm-6 col-md-6 col-lg-6 col-xl-6 float-left" style="padding: 0 0 0 0; !important;">
    <div class="top-bar">
        <br>
        <div class="row">
            <div class="col col-10 col-sm-10 col-md-8 col-lg-8 col-xl-8 float-left">
                <p class="path"><?php echo $json['file']; ?>\<span class="path-exception">Exception</span></p>
                <h3 class="title">Something broke!</h3>
            </div>

            <div class="col col-2 col-sm-2 col-md-4 col-lg-4 col-xl-4 float-right">
                <h1 class="exception-icon">:(</h1>
            </div>
        </div>
    </div>
    <div class="stack-bar">
        <p class="info">Stack frames (1):</p>
        <div class="stack-item">
            <div class="row stack-item-info">
                <div class="number">0</div>
                <a href="#" class="title"><?php echo $json['message']; ?></a>
            </div>
            <p class="location"><?php echo $json['file']; ?>:<?php echo $json['line']; ?></p>
        </div>
    </div>
</div>
<div class="col col-sm-6 col-md-6 col-lg-6 col-xl-6 float-right" style="padding: 0 0 0 0; !important;">
    <div class="code">
        <br><br>
        <p class="location"><?php echo $json['file']; ?></p>
        <div class="code-block">
                <pre>
                    <code class="prettyprint linenums:<?php echo (($json['line']+1) - 5) ?> lang-php code-text"><?php  echo calculateCodeBlock($json); ?></code>
                </pre>
        </div>
        <p class="info">No comments for this stackframe.</p>
    </div>
</div>


<a href="http://<?php echo $_SESSION['HTTP_REFERER']; ?>" class="float" title="Click here to go back">
<i class="fas fa-arrow-left my-float"></i>
</a>
</body>
</html>


<?php

function calculateCodeBlock($json) {
    $e = $json;
    $fh = fopen($json['file'], 'r');

    $code = array();
    foreach (file($json['file']) as $json['line']) {
        array_push($code, $json['line']);
    }

    $minLine = ($e['line']-1) - 5;
    $maxLine = ($e['line']-1) + 5;

    for($i = 0; $i < sizeof($code); $i++) {
        if ($i > $minLine && $i < $maxLine) {
          if ($i == ($e['line']-1)) {
            echo "<span class='error-line'>" . $code[$i] . "</span>";
          } else {
            echo $code[$i];
          }
        }
    }
}

 session_destroy();
 ?>
