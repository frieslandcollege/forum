-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 12 mei 2019 om 21:23
-- Serverversie: 10.1.37-MariaDB
-- PHP-versie: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`) VALUES
(1, 'Technology', '2019-02-14 13:48:30'),
(2, 'Gaming', '2019-02-14 13:48:30'),
(3, 'Auto', '2019-02-14 13:48:30'),
(4, 'Entertainment', '2019-02-14 13:48:30'),
(5, 'Books', '2019-02-14 13:48:30'),
(11, 'Fretten', '2019-05-09 11:44:16'),
(12, 'Test', '2019-05-09 20:17:14'),
(13, 'Programming', '2019-05-10 15:34:42'),
(14, 'Languages', '2019-05-11 22:38:27');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `login`
--

CREATE TABLE `login` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(3000) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `function` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `color` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `login`
--

INSERT INTO `login` (`ID`, `name`, `avatar`, `mail`, `password`, `username`, `role`, `function`, `about`, `color`, `active`, `admin`) VALUES
(6, 'Mark de waard', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'mail@mail.com', 'mark0001', 'Mark', 'USER', 'Tester', 'Hoi, ik ben het test account van Jesse!', '3B8A46', 0, 0),
(7, 'mark de waard', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'mail@mail.com', 'mark0001', '45678', 'USER', 'Gebruiker', '', '#ffffff', 0, 1),
(8, 'Jesse de Vries', 'http://localhost/School/Forum/data/images/26404221.jpg', 'mlgeditz1@gmail.com', '12345', 'MLGEditz', 'ADMIN', 'Proffesional moron', 'I\'m a programmer so i must be depressed.', '181C30', 1, 1),
(13, 'TestUser', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'testuser@example.com', 'Test', 'TestUser', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(17, 'Jesse', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'test@test.test', 'Test', 'Test', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(18, 'Jesse', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'jessedev020@outlook.com', 'Password123', 'Jessedev', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(19, 'Jesse', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'jessedev020@outlook.com', 'Password123', 'Jessedev', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(21, 'reader', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'test@test.com', '12345', 'musicman0001', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(22, 'Test', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'test@example.com', '12345', 'Test', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(23, 'Test', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'test@example.com', '12345', 'Test', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(24, 'Test', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'testuser@testuser.nl', 'Test', 'TestUser111', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(25, 'Jesse', 'https://i.ytimg.com/vi/_k5r_bJ3jCo/hqdefault.jpg', 'jessesyngaretestmail@gmail.com', '12345', 'Jessestesttroep', 'USER', 'Gebruiker', '', '#ffffff', 1, 1),
(26, 'ghost', 'https://i.ytimg.com/vi/-8kphJzz9hg/hqdefault.jpg', 'deepghost@pakot.nl', '12345', 'deepghost', 'ADMIN', 'Gebruiker', '', '#ffffff', 1, 1),
(27, 'rogue', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'rogue@pakot.nl', 'G#0$t', 'deeprogue', 'USER', 'Gebruiker', '', '#ffffff', 1, 0),
(29, 'Jesse', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'jesse@jesse.jesse', 'Goeieikbingurbefanautoruten', 'GutentagBratwurst', 'USER', '', '', '', 1, 0),
(30, 'Jesse', 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png', 'JFIOSEJ@fjIOEFJE.nl', 'Jesse', 'JIOJEF', 'USER', '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` longtext NOT NULL,
  `author` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_votes` mediumtext NOT NULL,
  `down_votes` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `body`, `author`, `created_at`, `up_votes`, `down_votes`) VALUES
(31, 13, 'Permission test', 'R3V0ZW50YWcgbWVpbiBsaWViZW48YnI+', '8', '2019-05-10 18:06:27', '6, 8', '');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexen voor tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT voor een tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
