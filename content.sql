-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 12 mei 2019 om 21:25
-- Serverversie: 10.1.37-MariaDB
-- PHP-versie: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `content`
--

CREATE TABLE `content` (
  `ID` int(10) NOT NULL,
  `Naam` varchar(255) NOT NULL DEFAULT 'Unknown',
  `Content` text NOT NULL,
  `Grootte` varchar(255) NOT NULL DEFAULT '20',
  `Kleur` varchar(255) NOT NULL DEFAULT '#000000',
  `Family` varchar(255) NOT NULL DEFAULT 'Roboto',
  `Weight` varchar(255) NOT NULL DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `content`
--

INSERT INTO `content` (`ID`, `Naam`, `Content`, `Grootte`, `Kleur`, `Family`, `Weight`) VALUES
(1, 'TEST', 'TG9yZW0gSXBzdW0gaXMgc2xlY2h0cyBlZW4gcHJvZWZ0ZWtzdCB1aXQgaGV0IGRydWtrZXJpai0gZW4gemV0dGVyaWp3ZXplbi4gTG9yZW0gSXBzdW0gaXMgZGUgc3RhbmRhYXJkIHByb2VmdGVrc3QgaW4gZGV6ZSBiZWRyaWpmc3RhayBzaW5kcyBkZSAxNmUgZWV1dywgdG9lbiBlZW4gb25iZWtlbmRlIGRydWtrZXIgZWVuIHpldGhhYWsgbWV0IGxldHRlcnMgbmFtIGVuIHplIGRvb3IgZWxrYWFyIGh1c3NlbGRlIG9tIGVlbiBmb250LWNhdGFsb2d1cyB0ZSBtYWtlbi4gSGV0IGhlZWZ0IG5pZXQgYWxsZWVuIHZpamYgZWV1d2VuIG92ZXJsZWVmZCBtYWFyIGlzIG9vaywgdnJpandlbCBvbnZlcmFuZGVyZCwgb3Zlcmdlbm9tZW4gaW4gZWxla3Ryb25pc2NoZSBsZXR0ZXJ6ZXR0aW5nLiBIZXQgaXMgaW4gZGUgamFyZW4gJzYwIHBvcHVsYWlyIGdld29yZGVuIG1ldCBkZSBpbnRyb2R1Y3RpZSB2YW4gTGV0cmFzZXQgdmVsbGVuIG1ldCBMb3JlbSBJcHN1bSBwYXNzYWdlcyBlbiBtZWVyIHJlY2VudGVsaWprIGRvb3IgZGVza3RvcCBwdWJsaXNoaW5nIHNvZnR3YXJlIHpvYWxzIEFsZHVzIFBhZ2VNYWtlciBkaWUgdmVyc2llcyB2YW4gTG9yZW0gSXBzdW0gYmV2YXR0ZW4u', '26', 'FFCF33', 'Roboto', 'normal'),
(2, 'WELCOME', 'V2Vsa29tLCBKZXNzZSBkZSBWcmllcw==', '32', '000000', 'Roboto', 'normal'),
(3, 'POSTS_TITLE', 'UG9zdHM=', '30', '3973AD', 'Roboto', 'normal'),
(4, 'POSTS_SUBTITLE', 'TGVlcyB6ZSBhbGxlbWFhbCE=', '13', '343434', 'Roboto', 'normal'),
(5, 'POSTS_TABLE_TITLE', 'VGl0bGU=', '16', '212529', 'Roboto', 'normal'),
(6, 'POSTS_TABLE_CATEGORY', 'Q2F0ZWdvcmll', '16', '212529', 'Roboto', 'normal'),
(7, 'POSTS_TABLE_AUTHOR', 'QXV0ZXVy', '16', '212529', 'Roboto', 'normal'),
(8, 'POSTS_TABLE_DATE', 'RGF0dW0=', '16', '212529', 'Roboto', 'normal'),
(9, 'POST_WRITER_TITLE', 'U2NocmlqdmVy', '30', '3973AD', 'Roboto', 'normal'),
(10, 'POST_WRITER_VISITBTN', 'QmV6b2VrZW4=', '16', 'FFFFFF', 'Roboto', 'normal'),
(11, 'ADMINPANEL_TITLE', 'QWRtaW5wYW5lbA==', '30', 'FFFFFF', 'Roboto', 'normal'),
(12, 'ADMINPANEL_SUBTITLE', 'SG91ZCBqaWogaGV0IGluIGRlIGdhdGVuPw==', '16', 'FFFFFF', 'Roboto', 'normal'),
(13, 'USERS_TITLE', 'R2VicnVpa2Vycw==', '30', '3973AD', 'Roboto', 'normal'),
(14, 'USERS_SUBTITLE', 'SGllciBsdWlzdGVyZW4gemUgbmFhciBqZSE=', '16', 'FFFFFF', 'Roboto', 'normal'),
(15, 'USERS_TABLE_ICON', 'SWNvbg==', '16', '212529', 'Roboto', 'normal'),
(16, 'USERS_TABLE_ID', 'SUQ=', '16', '212529', 'Roboto', 'normal'),
(17, 'USERS_TABLE_NAME', 'TmFhbQ==', '16', '212529', 'Roboto', 'normal'),
(18, 'USERS_TABLE_MAIL', 'RW1haWw=', '16', '212529', 'Roboto', 'normal'),
(19, 'USERS_TABLE_PASSWORD', 'V2FjaHR3b29yZA==', '16', '212529', 'Roboto', 'normal'),
(20, 'USERS_TABLE_USERNAME', 'R2VicnVpa2Vyc25hYW0=', '16', '212529', 'Roboto', 'normal'),
(21, 'USERS_TABLE_ROLE', 'Um9s', '16', '212529', 'Roboto', 'normal'),
(22, 'USERS_TABLE_ACTIONS', 'QWN0aWVz', '16', '212529', 'Roboto', 'normal'),
(23, 'CONTENT_TITLE', 'Q29udGVudA==', '30', '3973AD', 'Roboto', 'normal'),
(24, 'CONTENT_SUBTITLE', 'U2NocmVldXcgaGV0IHZhbiBkZSBkYWtlbg==', '16', '343434', 'Roboto', 'normal'),
(25, 'CONTENT_TABLE_ID', 'SUQ=', '16', '212529', 'Roboto', 'normal'),
(26, 'CONTENT_TABLE_TITLE', 'VGl0ZWw=', '16', '212529', 'Roboto', 'normal'),
(27, 'CONTENT_TABLE_CONTENT', 'Q29udGVudA==', '16', '212529', 'Roboto', 'normal'),
(28, 'CONTENT_TABLE_SIZE', 'VGV4dCBncm9vdHRl', '16', '212529', 'Roboto', 'normal'),
(29, 'CONTENT_TABLE_COLOR', 'VGV4dCBrbGV1cg==', '16', '212529', 'Roboto', 'normal'),
(30, 'CONTENT_TABLE_ACTIONS', 'QWN0aWVz', '16', '212529', 'Roboto', 'normal'),
(31, 'CONTENT_TABLE_EDITBTN', 'QmV3ZXJrZW4=', '16', 'FFFFFF', 'Roboto', 'normal'),
(32, 'CONTENT_TABLE_DELETEBTN', 'VmVyd2lqZGVyZW4=', '16', 'FFFFFF', 'Roboto', 'normal'),
(33, 'ADMIN_POSTS_TITLE', 'UG9zdHM=', '30', '3973AD', 'Roboto', 'normal'),
(34, 'ADMIN_POSTS_SUBTITLE', 'QmVoZWVyIHplIGFsbGVtYWFs', '16', '343434', 'Roboto', 'normal'),
(35, 'ADMIN_POSTS_TABLE_ID', 'SUQ=', '16', '212529', 'Roboto', 'normal'),
(36, 'ADMIN_POSTS_TABLE_CATEGORY', 'Q2F0ZWdvcmll', '16', '212529', 'Roboto', 'normal'),
(37, 'ADMIN_POSTS_TABLE_TITLE', 'VGl0ZWw=', '16', '212529', 'Roboto', 'normal'),
(38, 'ADMIN_POSTS_TABLE_BODY', 'Qm9keQ==', '16', '212529', 'Roboto', 'normal'),
(39, 'ADMIN_POSTS_TABLE_AUTHOR', 'U2NocmlqdmVy', '16', '212529', 'Roboto', 'normal'),
(40, 'ADMIN_POSTS_TABLE_DATE', 'R2VwbGFhdHN0IG9w', '16', '212529', 'Roboto', 'normal'),
(41, 'ADMIN_POSTS_TABLE_ACTIONS', 'QWN0aWVz', '16', '212529', 'Roboto', 'normal'),
(42, 'NAV_TITLE', 'Rm9ydW0=', '18', '000000', 'Roboto', 'normal'),
(43, 'NAV_HOME', 'SG9tZQ==', '16', '000000', 'Roboto', 'normal'),
(44, 'NAV_POSTS', 'UG9zdHM=', '16', '000000', 'Roboto', 'normal'),
(45, 'NAV_ADMINPANEL', 'QWRtaW5wYW5lbA==', '16', '000000', 'Roboto', 'normal'),
(46, 'NAV_ADMIN_TITLE', 'QWRtaW5wYW5lbA==', '18', '000000', 'Roboto', 'normal'),
(47, 'NAV_ADMIN_HOME', 'SG9tZQ==', '16', '000000', 'Roboto', 'normal'),
(48, 'NAV_ADMIN_PANEL', 'UGFuZWw=', '16', '000000', 'Roboto', 'normal'),
(49, 'NAV_ADMIN_USERS', 'R2VicnVpa2Vycw==', '16', '000000', 'Roboto', 'normal'),
(50, 'NAV_ADMIN_CONTENT', 'Q29udGVudA==', '16', '000000', 'Roboto', 'normal'),
(51, 'NAV_ADMIN_POSTS', 'UG9zdHM=', '16', '000000', 'Roboto', 'normal'),
(52, 'NAV_SETTINGS', 'SW5zdGVsbGluZ2Vu', '16', '000000', 'Roboto', 'normal'),
(53, 'NAV_PROFILE', 'UHJvZmllbA==', '16', '000000', 'Roboto', 'normal'),
(54, 'NAV_LOGOUT', 'VWl0bG9nZ2Vu', '16', '000000', 'Roboto', 'normal'),
(55, 'CONTENT_TABLE_STYLE', 'VGVrc3Qgc3Rpamw=', '16', '212529', 'Roboto', 'normal'),
(56, 'CONTENT_TABLE_WEIGHT', 'VGVrc3QgZGlrdGU=', '16', '212529', 'Roboto', 'normal');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `content`
--
ALTER TABLE `content`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
