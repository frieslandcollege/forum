<?php session_start();
include "assets/php/handling/Dependencies.php";

?>
<html>
    <head>
        <title>Forum</title>

        <!-- Stylesheets (Bootstrap) -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Javascript (JQuery + Bootstrap) -->
        <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

        <!-- Custom Stylesheets -->
        <link rel="stylesheet" href="assets/css/main.css">
    </head>
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="jumbotron login-menu">
                    <h3 class="text-center titletext">Inloggen</h3>
                    <form action="assets/php/login.php" method="post">
                        <div class="form-group">
                            <label for="usrname" class="col-form-label">Gebruikersnaam:</label>
                            <input name="usrname" type="text" class="form-control" id="usrname" required>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Wachtwoord:</label>
                            <input name="password" type="password" class="form-control" id="password" required>
                        </div>
                        <p class='error'><?php if (isset($_SESSION['loginError'])) echo $_SESSION['loginError'] ?></p>
                        <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase">Inloggen</button>
                    </form>
                </div>
            </div>


            <div class="col-md-6">
                <div class="jumbotron register-menu">
                    <h3 class="titletext text-center">Registreren</h3>
                    <form action="assets/php/register.php" method="post">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Naam:</label>
                            <input name="name" type="text" class="form-control" id="name" required>
                        </div>
                        <div class="form-group">
                            <label for="mail" class="col-form-label">Emailadres:</label>
                            <input name="mail" type="email" class="form-control" id="mail" required>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Wachtwoord:</label>
                            <input name="password" type="password" class="form-control" id="password" required>
                        </div>
                        <div class="form-group">
                            <label for="usrname" class="col-form-label">Gebruikersnaam:</label>
                            <input name="usrname" type="text" class="form-control" id="usrname" required>
                        </div>
                        <p class='error'><?php if (isset($_SESSION['registerError'])) echo $_SESSION['registerError']; ?></p>
                        <button type="submit" class="btn btn-lg btn-danger btn-block text-uppercase">Registreren</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
<?php session_destroy(); ?>
