<?php
session_start();
include "../../assets/php/handling/Dependencies.php";
if (!isset($_GET['id'])) header("Location: " . $_SERVER['HTTP_REFERER']);

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);
$currUser = new User($_GET['id']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId']) header("Location: " . $config->getBaseURL());

?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Javascript (ColorCalculator) -->
    <script src="../../assets/js/colors.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">
</head>
<body style="background-color: <?php echo "#" . $currUser->getColor() ?>;">

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <?php echo $c->get('a class="navbar-brand"', 'NAV_TITLE'); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="../public"', 'NAV_HOME'); ?>
            </li>
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="posts.php"', 'NAV_POSTS'); ?>
            </li>

            <?php if ($user->isAdmin()) { ?>
                <li class="nav-item">
                    <?php echo $c->get('a class="nav-link" href="../admin"', 'NAV_ADMINPANEL'); ?>
                </li>
            <?php } ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php echo $c->get('a class="dropdown-item" href="settings.php"', 'NAV_SETTINGS'); ?>
                        <?php echo $c->get('a class="dropdown-item" href="profile.php?id=' . $user->getID() . '"', 'NAV_PROFILE'); ?>
                        <div class="dropdown-divider"></div>
                        <?php echo $c->get('a class="dropdown-item" href="' . $config->getBaseURL() . '"', 'NAV_LOGOUT'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h1>Account</h1>
                    <p>Bekijk hier het account van <?php echo $currUser->getName() ?></p>
                    <hr>
                </div>
            </div>
        </div>
        <div class="col col-md-12">
            <div class="row">
                <div class="col col-md-3">
                    <img class="float-center" style="width: 400px; height: 400px;" src="<?php echo $currUser->getIcon(); ?>" alt="">
                </div>
                <div class="col col-md-9">
                    <h2 style="color: <?php echo "#" . $currUser->getColor() ?>;"><strong><?php echo $currUser->getName() ?></strong></h2>
                    <hr>
                    <p><strong style="color: <?php echo "#" . $currUser->getColor() ?>;">Gebruikersnaam</strong>: <?php echo $currUser->getUsername() ?><br>
                        <strong style="color: <?php echo "#" . $currUser->getColor() ?>;">Emailadres</strong>: <?php echo $currUser->getMail() ?><br>
                        <strong style="color: <?php echo "#" . $currUser->getColor() ?>;">Functie</strong>: <?php echo $currUser->getFunction() ?></p>

                    <p><strong style="color: <?php echo "#" . $currUser->getColor() ?>;">Over mij</strong>:<br> <?php echo $currUser->getAbout() ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    getTextColor('<?php echo $currUser->getColor(); ?>', true);
</script>
</body>
</html>
