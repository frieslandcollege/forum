<?php
session_start();
include "assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId']) header("Location: " . $config->getBaseURL());
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Summernote -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">

    <!-- Custom Javascript -->
    <script type="text/javascript" src="../../assets/js/PostEvents.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <?php echo $c->get('a class="navbar-brand"', 'NAV_TITLE'); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="../public"', 'NAV_HOME'); ?>
            </li>
            <li class="nav-item active">
                <?php echo $c->get('a class="nav-link" href="posts.php"', 'NAV_POSTS'); ?>
            </li>

            <?php if ($user->isAdmin()) { ?>
                <li class="nav-item">
                    <?php echo $c->get('a class="nav-link" href="../admin"', 'NAV_ADMINPANEL'); ?>
                </li>
            <?php } ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php echo $c->get('a class="dropdown-item" href="settings.php"', 'NAV_SETTINGS'); ?>
                        <?php echo $c->get('a class="dropdown-item" href="profile.php?id=' . $user->getID() . '"', 'NAV_PROFILE'); ?>
                        <div class="dropdown-divider"></div>
                        <?php echo $c->get('a class="dropdown-item" href="' . $config->getBaseURL() . '"', 'NAV_LOGOUT'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card" style="width: 100%">
                <div class="card-body">
                    <h5 class="card-title titletext">Posten</h5>
                    <p class="card-text subtext">Schreeuw het van de daken!</p><br><br>
                    <hr>

                    <form method="post" action="../../assets/php/handling/Save.php?AUTHOR=<?php echo $user->getID(); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" id="title" name="TITLE" placeholder="Voer hier uw titel in.">
                        </div>

                        <div class="form-group">
                            <select class="form-control category-selector" name="CATEGORY">
                                <?php


                                $stmt = $conn->getConnection()->prepare("SELECT * FROM `categories`");
                                $stmt->execute();

                                while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<option>" . $result['name'] . "</option>";
                                }

                                echo "<option>Other</option>";
                                ?>
                            </select>
                        </div>

                        <div class="form-group select-input"></div>

                        <textarea name="BODY" id="summernote"></textarea>

                        <button style="margin-top: 1%;" class="btn btn-outline-success" type="submit">Opslaan</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card float-right" style="width: 100%; margin-right: 5%;">
                <div class="card-body">
                    <h5 class="card-title">Writer</h5>

                    <img src="<?php echo  $user->getIcon(); ?>" style="width: 250px; height: 250px; border-radius: 50%;" alt=""><br><br>
                    <h4 class="text-center"><?php echo $user->getName() ?></h4>

                    <div class="row">
                        <div class="col text-center">
                            <a type="button" class="btn btn-primary text-white">Bezoeken</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#summernote').summernote({
        placeholder: 'Type hier je bericht',
        tabsize: 2,
        height: 100
    });
</script>

</body>
</html>
