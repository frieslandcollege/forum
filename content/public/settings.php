<?php
session_start();
include "../../assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId']) header("Location: " . $config->getBaseURL());


if (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action === "edit") {
        if (count($_POST) >= 6) {
            $keys = array();
            $values = array();
            $uploadOk = 1;

            if (isset($_FILES['image']) && $_FILES['image']['name'] != null) {
                $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/School/Forum/data/images/";
                $target_file = $target_dir . basename($_FILES['image']['name']);

                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

                if ($_FILES["image"]["size"] > 500000) {
                    $uploadOk = 0;
                }

                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                    $uploadOk = 0;
                }

                if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    $uploadOk = 0;
                }

                $_POST['avatar'] = $config->getBaseURL() . "data/images/" . basename($_FILES['image']['name']);
            }

            if ($uploadOk == 1) {
                foreach ($_POST as $key => $value) {
                    array_push($keys, $key);
                    array_push($values, $value);
                }

                $conn->update("login", $keys, $values, "ID", $user->getID());
                echo '<div class="alert alert-success"><strong>WHOOP WHOOP!</strong> De data van ' . $user->getName() . ' is geupdate!</div>';
            } else {
                echo '<div class="alert alert-danger"><strong>DAAR GAAT IETS MIS!</strong> Er is iets misgegaan tijdens het uploaden van je afbeelding!</div>';
            }
        } else {
            echo '<div class="alert alert-danger"><strong>OEPS!</strong> Je moet <strong>Alle</strong> velden invullen!</div>';
        }
    }
}


function select($role) {
    GLOBAL $user;
    if (strtoupper($role) === strtoupper($user->getRole())) return 'selected';
    return "";
}
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Javascript (JSColor) -->
    <script src="../../assets/js/jscolor.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <?php echo $c->get('a class="navbar-brand"', 'NAV_TITLE'); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="../public"', 'NAV_HOME'); ?>
            </li>
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="posts.php"', 'NAV_POSTS'); ?>
            </li>

            <?php if ($user->isAdmin()) { ?>
                <li class="nav-item">
                    <?php echo $c->get('a class="nav-link" href="../admin"', 'NAV_ADMINPANEL'); ?>
                </li>
            <?php } ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php echo $c->get('a class="dropdown-item" href="settings.php"', 'NAV_SETTINGS'); ?>
                        <?php echo $c->get('a class="dropdown-item" href="profile.php?id=' . $user->getID() . '"', 'NAV_PROFILE'); ?>
                        <div class="dropdown-divider"></div>
                        <?php echo $c->get('a class="dropdown-item" href="' . $config->getBaseURL() . '"', 'NAV_LOGOUT'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h1>Instellingen</h1>
                    <p>Beheer hier je account</p>
                    <hr>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-md-12">
                    <form action="?user=<?php echo $user->getID(); ?>&action=edit" method="post" enctype="multipart/form-data">

                        <div class="col col-md-4" style="display: inline-block; float: right;">
                            <img style="width: 300px; height: 300px;" src="<?php echo $user->getIcon(); ?>" alt="">
                            <input type="file" id="image" name="image">
                        </div>

                        <div class="col col-md-8" style="display: inline-block; float: left; text-align: left;">
                            <div class="form-group">
                                <label for="name">Naam</label>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Vul naam in" value="<?php echo $user->getName() ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="username">Gebruikersnaam</label>
                                <input name="username" type="text" class="form-control" id="username" placeholder="Vul gebruikersnaam in" value="<?php echo $user->getUsername() ?>" required>
                            </div>

                            <div class="form-group">
                                <label for="mail">Emailadres</label>
                                <input name="mail" type="email" class="form-control" id="mail" placeholder="Vul emailadres in" value="<?php echo $user->getMail() ?>" required>
                            </div>

                            <div class="form-group">
                                <label for="function">Functie</label>
                                <input name="function" type="text" class="form-control" id="function" placeholder="Vul functie in" value="<?php echo $user->getFunction() ?>">
                            </div>

                            <div class="form-group">
                                <label for="about">Over mij</label>
                                <textarea name="about" class="form-control" id="about" placeholder="Vertel iets over jezelf"><?php echo $user->getAbout() ?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="color">Kleur</label>
                                <input name="color" class="form-control jscolor" id="color" placeholder="Vul Kleur in" value="<?php echo $user->getColor() ?>">
                            </div>



                            <button type="submit" class="btn btn-primary text-white"><i class="fas fa-save"></i>   Opslaan</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
