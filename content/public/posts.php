<?php
session_start();
include "../../assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId']) header("Location: " . $config->getBaseURL());
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">

    <!-- Custom Javascript -->
    <script type="text/javascript" src="../../assets/js/PostEvents.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <?php echo $c->get('a class="navbar-brand"', 'NAV_TITLE'); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="../public"', 'NAV_HOME'); ?>
            </li>
            <li class="nav-item active">
                <?php echo $c->get('a class="nav-link" href="posts.php"', 'NAV_POSTS'); ?>
            </li>

            <?php if ($user->isAdmin()) { ?>
                <li class="nav-item">
                    <?php echo $c->get('a class="nav-link" href="../admin"', 'NAV_ADMINPANEL'); ?>
                </li>
            <?php } ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php echo $c->get('a class="dropdown-item" href="settings.php"', 'NAV_SETTINGS'); ?>
                        <?php echo $c->get('a class="dropdown-item" href="profile.php?id=' . $user->getID() . '"', 'NAV_PROFILE'); ?>
                        <div class="dropdown-divider"></div>
                        <?php echo $c->get('a class="dropdown-item" href="' . $config->getBaseURL() . '"', 'NAV_LOGOUT'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card" style="width: 100%">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-md-10">
                            <?php echo $c->get('h5 class="card-title titletext"', 'POSTS_TITLE'); ?>
                            <?php echo $c->get('p class="card-text subtext"', 'POSTS_SUBTITLE'); ?><br><br>
                        </div>
                        <div class="col col-md-2">
                            <a href="create.php" type="button" title="Create new post" class="btn btn-warning text-white float-right"><i class="fas fa-pencil-alt"></i></a>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <?php echo $c->get('th', 'POSTS_TABLE_TITLE'); ?>
                                <?php echo $c->get('th', 'POSTS_TABLE_CATEGORY'); ?>
                                <?php echo $c->get('th', 'POSTS_TABLE_AUTHOR'); ?>
                                <?php echo $c->get('th', 'POSTS_TABLE_DATE'); ?>
                            </tr>
                        </thead>

                        <tbody>
                        <?php

                        $stmt = $conn->getConnection()->prepare("SELECT * FROM `posts`");
                        $stmt->execute();

                        if ($stmt->rowCount() < 1) {
                            echo "<h3>Momenteel zijn er geen posts beschikbaar.</h3>";
                        }

                        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            $post = new Posts($result["id"]);
                            $writer = new User($post->getAuthor());
                            echo "<tr class=\"post\" onclick=\"window.location.href='post.php?id=" . $post->getID() . "'\">";
                            echo "<td>" . $post->getTitle() . "</td>";
                            echo "<td>" . $post->getCategory() . "</td>";
                            echo "<td>" . $writer->getName() . "</td>";
                            echo "<td>" . $post->getDate() . "</td>";
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card float-right" style="width: 100%; margin-right: 5%;">
                <div class="card-body">
                    <h5 class="card-title titletext">Topics</h5>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
