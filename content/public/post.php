<?php
session_start();
include "../../assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);
$post = new Posts($_GET['id']);
$writer = new User($post->getAuthor());

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId'] || !isset($_GET['id'])) header("Location: " . $config->getBaseURL());
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Javascript (ColorCalculator) -->
    <script src="../../assets/js/colors.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">

    <!-- Custom Javascript -->
    <script type="text/javascript" src="../../assets/js/PostEvents.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <?php echo $c->get('a class="navbar-brand"', 'NAV_TITLE'); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <?php echo $c->get('a class="nav-link" href="../public"', 'NAV_HOME'); ?>
            </li>
            <li class="nav-item active">
                <?php echo $c->get('a class="nav-link" href="posts.php"', 'NAV_POSTS'); ?>
            </li>

            <?php if ($user->isAdmin()) { ?>
                <li class="nav-item">
                    <?php echo $c->get('a class="nav-link" href="../admin"', 'NAV_ADMINPANEL'); ?>
                </li>
            <?php } ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php echo $c->get('a class="dropdown-item" href="settings.php"', 'NAV_SETTINGS'); ?>
                        <?php echo $c->get('a class="dropdown-item" href="profile.php?id=' . $user->getID() . '"', 'NAV_PROFILE'); ?>
                        <div class="dropdown-divider"></div>
                        <?php echo $c->get('a class="dropdown-item" href="' . $config->getBaseURL() . '"', 'NAV_LOGOUT'); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card" style="width: 100%">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-md-10">
                            <h5 class="card-title"><?php echo $post->getTitle(); ?></h5>
                            <p class="card-text"><?php echo base64_decode($post->getBody()); ?></p><br><br>
                        </div>
                        <div class="col col-md-2">
                            <div class="btn-group float-right">
                                <?php if ($post->getAuthor() == $user->getID() || $user->isAdmin()) echo '<a href="edit.php?id=' . $post->getID() . '" type="button" title="Edit post" class="btn btn-warning text-white"><i class="fas fa-pencil-alt fa-fw"></i></a>';?>
                                <?php if ($post->getAuthor() == $user->getID() || $user->isAdmin()) echo '<a href="' . $config->getBaseURL() . "assets/php/handling/Delete.php?ID=" . $post->getID() . '" type="button" title="Delete post" class="btn btn-danger text-white"><i class="fas fa-trash fa-fw"></i></a>';?>
                            </div>
                        </div>
                    </div>
                    <div class="float-left">

                        <?php

                        $success = (hasUpvoted($post, $user)) ? "up_vote btn btn-success" : "up_vote btn btn-outline-success";
                        $danger = (hasDownvoted($post, $user)) ? "down_vote btn btn-danger" : "down_vote btn btn-outline-danger";

                        ?>

                        <button type="button" onclick="vote('UP', '<?php echo $user->getID(); ?>', '<?php echo $post->getID(); ?>')" class="<?php echo $success; ?>"><?php echo getUpvotes($post); ?></button>

                        <button type="button" onclick="vote('DOWN', '<?php echo $user->getID(); ?>', '<?php echo $post->getID(); ?>')" class="<?php echo $danger; ?>"><?php echo getDownvotes($post); ?></button>
                    </div>
                    <p class="float-right"><?php echo $post->getDate(); ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card float-right" style="width: 100%; margin-right: 5%;">
                <div class="card-body">
                    <h5 class="card-title titletext">Writer</h5>

                    <img src="<?php echo  $writer->getIcon(); ?>" style="width: 250px; height: 250px; border-radius: 50%;" alt=""><br><br>
                    <h4 class="text-center"><?php echo $writer->getName() ?></h4>

                    <div class="row">
                        <div class="col text-center">
                            <a href="profile.php?id=<?php echo $writer->getID(); ?>" type="button" class="btn btn-primary visit_btn" style="background-color: <?php echo "#" . $writer->getColor() ?>; border-color: <?php echo "#" . $writer->getColor() ?>">Bezoeken</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    getTextColor('<?php echo $writer->getColor(); ?>', false);
</script>
</body>
</html>


<?php

function getUpvotes($post) {
    $result = $post->getUpvotes();
    $array = explode(", ", $result);

    $amount = 0;
    for ($i = 0; $i < sizeof($array); $i++) {
        if (!empty($array[$i]) || $array[$i] != null) $amount++;
    }

    return $amount;
}

function getDownvotes($post) {
    $result = $post->getDownvotes();
    $array = explode(", ", $result);

    $amount = 0;
    for ($i = 0; $i < sizeof($array); $i++) {
        if (!empty($array[$i]) || $array[$i] != null) $amount++;
    }

    return $amount;
}

function hasUpvoted($post, $user) {
    $result = $post->getUpvotes();
    $array = explode(", ", $result);

    if (sizeof($array) < 1) return false;
    return in_array($user->getID(), $array);
}

function hasDownvoted($post, $user) {
    $result = $post->getDownvotes();
    $array = explode(", ", $result);

    if (sizeof($array) < 1) return false;
    return in_array($user->getID(), $array);
}

?>
