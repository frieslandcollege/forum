<?php
session_start();
include "../../assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId'] || !$user->isAdmin()) header("Location: " . $config->getBaseURL());
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">AdminPanel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="../public"> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php"> Panel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="users.php"> Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="content/index.php"> Content</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="posts.php"> Posts</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../public/settings.php">Instellingen</a>
                        <a class="dropdown-item" href="../public/profile.php?id=<?php echo $user->getID() ?>">Profiel</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo $config->getBaseURL(); ?>">Uitloggen</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="jumbotron user-menu">
        <div class="col-md-12">
            <h5 class="titletext">Posts</h5>
            <p class="subtext">Beheer ze allemaal</p><br><br>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Categorie</th>
                    <th scope="col">Titel</th>
                    <th scope="col">Body</th>
                    <th scope="col">Auteur</th>
                    <th scope="col">Geplaatst op</th>
                    <th scope="col">Acties</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $limit = 5; //Amount of users per page
                $adjacents = 2;

                $stmt = $conn->getConnection()->prepare("SELECT * FROM `posts`");
                $stmt->execute();

                $total_rows = $stmt->rowCount();
                $total_pages = ceil($total_rows / $limit);

                if(isset($_GET['page']) && $_GET['page'] != "") {
                    $page = $_GET['page'];
                    $offset = $limit * ($page-1);
                } else {
                    $page = 1;
                    $offset = 0;
                }

                $query = $conn->getConnection()->prepare("SELECT * FROM `posts` LIMIT " . $offset . ", " . $limit);
                $query->execute();

                if ($query->rowCount() > 0) {

                    //Loop through all the results
                    while ($result = $query->fetch(PDO::FETCH_ASSOC)) {
                        $post = new Posts($result["id"]);

                        echo "
                                    <tr>
                                    
                                    <td>" . $post->getID() . "</td>
                                    <td>" . $post->getCategory() . "</td>
                                    <td>" . $post->getTitle() . "</td>
                                    <td>" . substr(base64_decode($post->getBody()), 0, 99) . "</td>
                                    <td>" . $post->getAuthor() . "</td>
                                    <td>" . $post->getDate() . "</td>
                                    <td><a type='button' href='../public/post.php?id=" . $post->getID() . "' title='Bekijken' class='btn btn-info'><i class='fas fa-eye'></i></a> <a type='button' href='../public/edit.php?id=" . $post->getID() . "' title='Bewerken' class='btn btn-warning'><i class='fas fa-pencil-alt text-white'></i></i></a> <a type='button' href='" . $config->getBaseURL() . "assets/php/handling/Delete.php?ID=" . $post->getID() . "'  title='Verwijderen' class='btn btn-danger'><i class='fas fa-trash'></i></a></td>
                                    
                                    </tr>
                                ";
                    }
                }

                if($total_pages <= (1+($adjacents * 2))) {
                    $start = 1;
                    $end   = $total_pages;
                } else {
                    if(($page - $adjacents) > 1) {
                        if(($page + $adjacents) < $total_pages) {
                            $start = ($page - $adjacents);
                            $end   = ($page + $adjacents);
                        } else {
                            $start = ($total_pages - (1+($adjacents*2)));
                            $end   = $total_pages;
                        }
                    } else {
                        $start = 1;
                        $end   = (1+($adjacents * 2));
                    }
                }
                ?>
                </tbody>
            </table>


            <?php if($total_pages > 1) { ?>
                <ul class="pagination justify-content-center">

                    <!-- Link of the previous page -->
                    <li class='page-item <?php ($page <= 1 ? print 'disabled' : '')?>'>
                        <a class='page-link' href='?page=<?php ($page>1 ? print($page-1) : print 1)?>' tabindex="-1">Previous</a>
                    </li>

                    <!-- Links of the pages with page number -->
                    <?php for($i=$start; $i<=$end; $i++) { ?>
                        <li class='page-item <?php ($i == $page ? print 'active' : '')?>'>
                            <a class='page-link' href='?page=<?php echo $i;?>'><?php echo $i;?></a>
                        </li>
                    <?php } ?>

                    <!-- Link of the next page -->
                    <li class='page-item <?php ($page >= $total_pages ? print 'disabled' : '')?>'>
                        <a class='page-link' href='?page=<?php ($page < $total_pages ? print($page+1) : print $total_pages)?>'>Next</a>
                    </li>
                </ul>
            <?php } ?>

        </div>
    </div>
</div>

</body>
</html>
