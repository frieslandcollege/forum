<?php
session_start();
include "../../assets/php/handling/Dependencies.php";
if (!isset($_GET['user'])) header("Location: users.php");

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);
$currUser = new User($_GET['user']);

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId'] || !$user->isAdmin()) header("Location: " . $config->getBaseURL());


if (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action === "edit") {
        if (count($_POST) >= 5) {
            $keys = array();
            $values = array();
            foreach ($_POST as $key => $value) {
                array_push($keys, $key);
                array_push($values, $value);
            }
            $conn->update("login", $keys, $values, "ID", $currUser->getID());
            echo '<div class="alert alert-success"><strong>WHOOP WHOOP!</strong> De data van ' . $currUser->getName() . ' is geupdate! Klik <strong><a href="users.php" style="text-decoration: none; color: #272526">hier</a></strong> om terug te gaan.</div>';
        } else {
            echo '<div class="alert alert-danger"><strong>OEPS!</strong> Je moet <strong>Alle</strong> velden invullen!</div>';
        }
    }
}


function select($role) {
    GLOBAL $currUser;
    if (strtoupper($role) === strtoupper($currUser->getRole())) return 'selected';
    return "";
}
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">AdminPanel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="../public"> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php"> Panel</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="users.php"> Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="content/index.php"> Content</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="posts.php"> Posts</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../public/settings.php">Instellingen</a>
                        <a class="dropdown-item" href="../public/profile.php?id=<?php echo $user->getID() ?>">Profiel</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo $config->getBaseURL() ?>">Uitloggen</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h1>Bewerken</h1>
                    <p>Beheer hier het account van <?php echo $currUser->getName()  ?></p>
                    <hr>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-md-12">

                <div class="col col-md-4" style="display: inline-block; float: right;">
                    <img style="width: 300px; height: 300px;" src="<?php echo $currUser->getIcon(); ?>" alt="">
                </div>

                <div class="col col-md-8" style="display: inline-block; float: left; text-align: left;">
                    <form action="user.php?user=<?php echo $currUser->getID(); ?>&action=edit" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Vul naam in" value="<?php echo $currUser->getName() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Gebruikersnaam</label>
                            <input name="username" type="text" class="form-control" id="username" placeholder="Vul gebruikersnaam in" value="<?php echo $currUser->getUsername() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="mail">Emailadres</label>
                            <input name="mail" type="email" class="form-control" id="mail" placeholder="Vul emailadres in" value="<?php echo $currUser->getMail() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Wachtwoord</label>
                            <input name="password" type="password" class="form-control" id="password" placeholder="Vul wachtwoord in" value="<?php echo $currUser->getPassword() ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Rol</label>
                            <select name="role" class="form-control" id="role" value="<?php echo $currUser->getRole() ?>" required>
                                <option <?php echo select("USER");?>>USER</option>
                                <option <?php echo select("READER");?>>READER</option>
                                <option <?php echo select("WRITER");?>>WRITER</option>
                                <option <?php echo select("ADMIN");?>>ADMIN</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary text-white"><i class="fas fa-save"></i>   Opslaan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
