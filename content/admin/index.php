<?php
session_start();
include "../../assets/php/handling/Dependencies.php";

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);
$content = new Content(null);

if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId'] || !$user->isAdmin()) header("Location: " . $config->getBaseURL());
$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Javascript (Charts) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/main.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">AdminPanel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="../public"> Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="index.php"> Panel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="users.php"> Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="content/index.php"> Content</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="posts.php"> Posts</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../public/settings.php">Instellingen</a>
                        <a class="dropdown-item" href="../public/profile.php?id=<?php echo $user->getID() ?>">Profiel</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo $config->getBaseURL(); ?>">Uitloggen</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col col-md-12 no-card">
            <?php echo $content->get("h5 class='card-title titletext-dark'", "ADMINPANEL_TITLE")?>
            <?php echo $content->get("p class='card-text subtext-dark'", "ADMINPANEL_SUBTITLE")?><br><br>
        </div>
    </div>

    <div class="row d-flex justify-content-center mt-5">
        <div class="col-md-6">
            <canvas id="lineChart"><h1>Waiting...</h1></canvas>
        </div>
    </div>
</div>

<!--<div class="container">-->
<!--    <div class="col-md-12">-->
<!--        <h5 class="titletext">Users</h5>-->
<!--        <p class="subtext">Hier luisteren ze naar je</p><br><br>-->
<!--    </div>-->
<!---->
<!---->
<!--    <div class="row d-flex justify-content-center mt-5">-->
<!--        <div class="col-md-6">-->
<!--            <canvas id="lineChart"><h1>Waiting...</h1></canvas>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!-- Custom Javascript -->
<script src="../../assets/js/PostChart.js"></script>
</body>
</html>
