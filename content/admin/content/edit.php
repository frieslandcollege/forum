<?php
session_start();
include "../../../assets/php/handling/Dependencies.php";
if (!isset($_GET['content']))header("Location: index.php");

$conn = new Connection();
$config = new Config();
$user  = new User($_SESSION['userId']);
$content = new Content((int) $_GET['content']);
$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

if (!isset($_SESSION['loggedIn']) || !$_SESSION['userId'] || !$user->isAdmin()) header("Location: " . $config->getBaseURL());


if (isset($_GET['action']) && $_GET['action'] == "edit") {
    $keys = array("Naam", "Content", "Grootte", "Kleur", "Family", "Weight");
    $values = array($_POST['Naam'], base64_encode($_POST['Content']), $_POST['Grootte'], $_POST['Kleur'], $_POST['Family'], $_POST['Weight']);
    $conn->update("content", $keys, $values, "ID", $content->getID());
    header("Location: index.php");
}
?>

<html>
<head>
    <title>Forum</title>

    <!-- Stylesheets (Bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript (JQuery + Bootstrap) -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Javascript (JSColor) -->
    <script src="../../../assets/js/jscolor.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="../../../assets/css/main.css">

    <!-- JavaScript (Custom -->
    <script src="../../../assets/js/contentEditor.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">AdminPanel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="../../public"> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../index.php"> Panel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../users.php"> Users</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="index.php"> Content</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../posts.php"> Posts</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo $user->getIcon(); ?>" alt="" class="avatar">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../../public/settings.php">Instellingen</a>
                        <a class="dropdown-item" href="../../public/profile.php?id=<?php echo $user->getID() ?>">Profiel</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo $config->getBaseURL() ?>">Uitloggen</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h1 class="titletext">Content</h1>
                    <p class="subtext">Bewerk het hier</p>
                    <hr>
                    <i style="font-size: 12px;">*Het gebruik van HTML is mogelijk</i><br><br>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-md-12">
                <form action="?content=<?php echo $content->getID(); ?>&action=edit" method="post">

                    <div class="col col-md-4" style="display: inline-block; float: right;">
                        <div class="preview">
                            <h5 id="preview-text" style="font-size: <?php echo $content->getTextSize(); ?>px; color: <?php echo "#" . $content->getTextColor(); ?>;"><?php echo $content->getContent(); ?></h5>
                        </div>
                    </div>

                    <div class="col col-md-8" style="display: inline-block; float: left; text-align: left;">
                        <div class="form-group">
                            <label for="name">Titel</label>
                            <input name="Naam" type="text" class="form-control" id="name" placeholder="Vul een Titel in" value="<?php echo $content->getName(); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="Content" class="form-control" id="content" placeholder="Wat moet er hier komen te staan?" required><?php echo $content->getContent() ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="size">Text grootte</label>
                            <input name="Grootte" type="number" class="form-control" id="size" placeholder="Kies de grootte van je text" value="<?php echo $content->getTextSize() ?>" max="80" min="10" required>
                        </div>

                        <div class="form-group">
                            <label for="family">Stijl</label>
                            <select class="form-control" id="family" name="Family">
                                <option <?php if ($content->getFontFamily() == "Roboto") echo "selected"; ?>>Roboto</option>
                                <option <?php if ($content->getFontFamily() == "Arial") echo "selected"; ?>>Arial</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="family">Dikte</label>
                            <select class="form-control" id="weight" name="Weight">
                                <option <?php if ($content->getFontWeight() == "lighter") echo "selected"; ?>>lighter</option>
                                <option <?php if ($content->getFontWeight() == "normal") echo "selected"; ?>>normal</option>
                                <option <?php if ($content->getFontWeight() == "bold") echo "selected"; ?>>bold</option>
                                <option <?php if ($content->getFontWeight() == "bolder") echo "selected"; ?>>bolder</option>
                                <option <?php if ($content->getFontWeight() == "inherit") echo "selected"; ?>>inherit</option>
                                <option <?php if ($content->getFontWeight() == "initial") echo "selected"; ?>>initial</option>
                                <option <?php if ($content->getFontWeight() == "revert") echo "selected"; ?>>revert</option>
                                <option <?php if ($content->getFontWeight() == "unset") echo "selected"; ?>>unset</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="color">Kleur</label>
                            <input name="Kleur" class="form-control jscolor" id="color" placeholder="Kies een kleur" value="<?php echo $content->getTextColor() ?>" required>
                        </div>



                        <button type="submit" class="btn btn-primary text-white"><i class="fas fa-save"></i>   Opslaan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</body>
</html>
