$(document).ready(function() {

    $(".post").on('mouseover', function() {
        $(this).css("background-color", "#cdcdcd");
        $(this).css('cursor', 'pointer');
    });

    $(".post").on('mouseout', function() {
        $(this).css("background-color", "#ffffff");
        $(this).css("cursor", "default");
    });


    $(".category-selector").change(function() {
        $( ".category-selector option:selected" ).each(function() {
           let text = $(this).text();
            let input = $("<input/>", {
                type: 'text',
                class: 'form-control',
                id: 'custom-category',
                name: 'CUSTOM_CATEGORY',
                placeholder: 'Voer uw categorie in.'
            });

           if (text.toUpperCase() === "OTHER") {
               $(".select-input").append(input);
           } else {
               $("#custom-category").remove();
           }
        });
    });

});



function vote(vote, voteUser, post) {
    $.ajax({
        type: "POST",
        url: "http://localhost/School/Forum/assets/php/handling/Vote.php",
        data: {
            vote: vote,
            user: voteUser,
            post: post
        }
    }).done(function (msg) {
        window.location.reload();
    }).fail(function (msg) {
        console.log("Voting failed: " + msg);
    });
}