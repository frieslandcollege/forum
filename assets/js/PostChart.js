$(window).on('load', function() {
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var gradientFill = ctxL.createLinearGradient(0, 0, 0, 290);
    gradientFill.addColorStop(0, "rgba(173, 53, 186, 1)");
    gradientFill.addColorStop(1, "rgba(173, 53, 186, 0.1)");
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: getDateStrings(7),
            datasets: [
                {
                    label: "Posts",
                    data: getPosts(),
                    backgroundColor: gradientFill,
                    borderColor: [
                        '#AD35BA',
                    ],
                    borderWidth: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "rgba(173, 53, 186, 0.1)",
                }
            ]
        },
        options: {
            responsive: true
        }
    });

    function getPosts() {
        let data = JSON.parse(stripScripts(getPostData()));
        let dates = getDates(7);
        console.log(dates);
        let array = [];

        for (let i = 0; i < dates.length; i++) {
            let currAmount = 0;
            for (let j = 0; j < data.length; j++) {
                console.log(dates[i], data[j]['created_at'].substring(0, 10));
                if (dates[i] === data[j]['created_at'].substring(0, 10)) {
                    currAmount++;
                }
            }
            array.push(currAmount);
        }
        return array;
    }

    function getDates(days) {
        let array = [];

        for (let i = days; i >= 0; i--) {
            let date = new Date();
            date.setDate(date.getDate() - i);

            let yy = date.getFullYear();
            let mm = ((parseInt(date.getMonth())+1).toString().length < 2) ? "0" + (parseInt(date.getMonth())+1) : (parseInt(date.getMonth())+1);
            let dd = (date.getDate().toString().length < 2) ? "0" + date.getDate() : date.getDate();

            let today = yy + "-" + mm + "-" + dd;
            array.push(today);
        }
        return array;
    }

    function getDateStrings(days) {
        let array = [];

        for (let i = days; i >= 0; i--) {
            let date = new Date();
            date.setDate(date.getDate() - i);
            array.push(date.toDateString());
        }
        return array;
    }

    function getPostData() {
        let response = "Something went wrong";
        $.ajax({
            type: "GET",
            url: "http://localhost/School/Forum/assets/php/handling/PostData.php",
            async: false,
            success: function (msg) {
                response = msg;
            }
        });

        return response;
    }

    function stripScripts(s) {
        var div = document.createElement('div');
        div.innerHTML = s;
        var scripts = div.getElementsByTagName('script');
        var i = scripts.length;
        while (i--) {
            scripts[i].parentNode.removeChild(scripts[i]);
        }
        return div.innerHTML;
    }
});