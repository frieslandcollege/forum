<?php
class Config {

    private $host = "localhost";
    private $username = "root";
    private $password = "password";
    private $database = "database";

    public function __construct() {

    }

    public function getHost() {
        return $this->host;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getDatabase() {
        return $this->database;
    }

    public function getBaseURL() {
        $URL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $array = explode('/', $URL);

        $baseURL = "http://";

        for ($i = 0; $i < sizeof($array); $i++) {
            if (strtoupper($array[$i]) != "FORUM") {
                $baseURL .= $array[$i] . "/";
            } else {
                $baseURL .= $array[$i] . "/";
                break;
            }
        }

        return $baseURL;

    }
}
