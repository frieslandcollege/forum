<?php
class Content {

    private $text;
    private $conn;
    private $key;

    public function __construct($text) {
        $this->conn = new Connection();
        $config = new Config();
        if($text != null) {
            if (is_integer($text)) $this->key = "ID";
            else $this->key = "Naam";
            $this->text = $text;
        }
        $this->conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
    }

    public function getKey() {
        return $this->key;
    }

    public function getText() {
        return $this->text;
    }

    public function getID() {
        return $this->conn->get("content", $this->key, $this->text, "ID");
    }

    public function getName() {
        return $this->conn->get("content", $this->key, $this->text, "Naam");
    }

    public function getContent() {
        return base64_decode($this->conn->get("content", $this->key, $this->text, "Content"));
    }

    public function getTextSize() {
        return $this->conn->get("content", $this->key, $this->text, "Grootte");
    }

    public function getTextColor() {
        return $this->conn->get("content", $this->key, $this->text, "Kleur");
    }

    public function getFontFamily() {
        return $this->conn->get("content", $this->key, $this->text, "Family");
    }

    public function getFontWeight() {
        return $this->conn->get("content", $this->key, $this->text, "Weight");
    }



    public function setID($value) {
        $this->conn->update("content", array("ID"), array($value), $this->key, $this->text);
    }

    public function setName($value) {
        $this->conn->update("content", array("Naam"), array($value), $this->key, $this->text);
    }

    public function setContent($value) {
        $this->conn->update("content", array("Content"), array($value), $this->key, $this->text);
    }

    public function setTextSize($value) {
        $this->conn->update("content", array("Grootte"), array($value), $this->key, $this->text);
    }

    public function setTextColor($value) {
        $this->conn->update("content", array("Kleur"), array($value), $this->key, $this->text);
    }


    public function add($name, $content, $size, $color) {
        $keys = array($name, base64_encode($content), $size, $color);
        $values = array("Naam", "Content", "Grootte", "Kleur");

        $this->conn->insert("content", $keys, $values);
    }

    public function delete() {
        $this->conn->delete("content", $this->key, $this->text);
    }

    public function get($element, $text) {
        if($text != null) {
            if (is_integer($text)) $this->key = "ID";
            else $this->key = "Naam";
            $this->text = $text;
        }

        $color = "#" . $this->getTextColor();
        return "<" . $element .  " style='font-size: " . $this->getTextSize() . "px; font-family: " . $this->getFontFamily() . "; font-weight: " . $this->getFontWeight() . "; color: " . $color . "'>" . $this->getContent() . "</" . $element .  ">";
    }


}