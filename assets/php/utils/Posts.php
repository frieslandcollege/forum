<?php
class Posts {

    private $post;
    private $conn;

    public function __construct($post) {
        $this->conn = new Connection();
        $config = new Config();
        $this->post = $post;
        $this->conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
    }

    public function getID() {
        return $this->conn->get("posts", "id", $this->post, "id");
    }

    public function getCategory() {
        $id = $this->conn->get("posts", "id", $this->post, "category_id");
        return $this->conn->get("categories", "id", $id, "name");
    }

    public function getTitle() {
        return $this->conn->get("posts", "id", $this->post, "title");
    }

    public function getBody() {
        return $this->conn->get("posts", "id", $this->post, "body");
    }

    public function getAuthor() {
        return $this->conn->get("posts", "id", $this->post, "author");
    }

    public function getDate() {
        return $this->conn->get("posts", "id", $this->post, "created_at");
    }

    public function getUpvotes() {
        return $this->conn->get("posts", "id", $this->post, "up_votes");
    }

    public function setUpvotes($value) {
        $this->conn->update("posts", array("up_votes"), array($value), "id", $this->post);
    }

    public function getDownvotes() {
        return $this->conn->get("posts", "id", $this->post, "down_votes");
    }

    public function setDownvotes($value) {
        $this->conn->update("posts", array("down_votes"), array($value), "id", $this->post);
    }



}