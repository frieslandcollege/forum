<?php
class Connection {

    private $connection;

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Constructor of the connection class
     * @constructor
     */
    public function __construct() {
        //Constructor will be updated later if necessary
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Open a new connection to the database
     * @param $host String -> Host of the database (Example: localhost)
     * @param $username -> Username of the database (Example: root)
     * @param $password -> Password of the database (Example: password)
     * @param $database -> Database you want to connect to (Example: database)
     */
    public function connect($host, $username, $password, $database) {
        //Checks if there is already a connection
        if ($this->connection != null) {
            $this->log("Could not open a new connection. You need to close your current connection first!", true, true); //Logs an error which says you need to close the connection first
            return; //Return the code to prevent it from doing other stuff
        }

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$database", $username, $password); //Create a new connection
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Set attributes for PDO. (Enabling error reporting and lets it throw exceptions
            $this->log("Connected successfully to " . $host . ":" . $database, true, false); //Logs an message which says that the connection was successful
        } catch (PDOException $e) {
          $this->log("Connection failed: " . $e->getMessage(), true, true); //Logs an error which says the connection failed
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Close the connection to the database
     */
    public function close() {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not close connection. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return; //Return the code to prevent it from doing other stuff
        }
        $this->connection = null; //Closes the connection
        $this->log("Connection has been closed successfully", true, false); //Logs a message which says the connection was closed successfully
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Create a new database
     * @param $name String -> Name of the database you want to create
     */
    public function createDatabase($name) {
        try {
            $stmt = $this->connection->prepare("CREATE DATABASE IF NOT EXISTS `" . $name . "`"); //Prepare a query to create the database
            $stmt->execute(); //Execute the statement

            $this->log("Database " . $name . " created successfully", true, false); //Logs a message which says the database was created
        } catch (PDOException $e) {
            $this->log("Could not create database " . $name . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while creating a database
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Create a new Table
     * @param $name String -> Name of the table you want to create
     * @param $array array -> Name of the columns you want to add. (Example: ['ID', 'name', 'mail', 'password', 'username'])
     */
    public function createTable($name, $array) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not create a table. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return; //Return the code to prevent it from doing other stuff
        }

        try {
            $data = ""; //Create a new variable where the options string will be stored in

            //Loop through the array
            for($a=0; $a < sizeof($array); $a++) {
                $type = (strtoupper($array[$a]) == "ID") ? "INT(10) AUTO_INCREMENT PRIMARY KEY" : "VARCHAR(255)"; //Calculate the options for the column

                //Check if it is the first column to add. This way it will not add an comma at the beginning
                if ($data == "") $data = $array[$a] . " " . $type;
                else $data .= ", " . $array[$a] . " " . $type;
            }

            $stmt = $this->connection->prepare("CREATE TABLE IF NOT EXISTS `" . $name . "` ($data)"); //Prepare an statement to create the table
            $stmt->execute(); //Execute the prepared statement

            $this->log("Table " . $name . " created successfully", true, false); //Logs a message which says the table was created successfully
        } catch (PDOException $e) {
            $this->log("Could not create table " . $name . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while creating the table
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Insert the data into the database
     * @param $table String -> The table where the data is stored
     * @param $keys array -> Array with all the keys you want to add in the database (Example: ['name', 'mail', 'password', 'username'])
     * @param $values array -> Array with all the values you want to add in the database (Example: ['Jesse', 'jesse@example.com', '12345', 'Test'])
     */
    public function insert($table, $keys, $values) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not insert data. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return; //Return the code to prevent it from doing other stuff
        }

        if (sizeof($keys) != sizeof($values)) {
            $this->log("Could not insert data. The number of keys and values ​​must be equal to each other", true, true); //Logs an error which says the number of keys and values needs to be equal to each other.
            return; //Return the code to prevent it from doing other stuff
        }

        try {
            $newKeys = ""; //Create a new variable where all the keys will be stored in
            $newValues = ""; //Create a new variable where all the values stored in the parameters will be stored in
            $data = ""; //Create a new variable where all the new will be stored in

            //Loop through all the keys to create a key string in $newKeys
            for ($i=0; $i < sizeof($keys); $i++) {
                if ($newKeys == "") $newKeys = $keys[$i];
                else $newKeys .= ", " . $keys[$i];
            }

            //Loop through all the keys to create a parameter string in $newValues (The difference is the : in front of the parameter)
            for ($i=0; $i < sizeof($keys); $i++) {
                if ($newValues == "") $newValues = ":" . $keys[$i];
                else $newValues .= ", :" . $keys[$i];
            }

            //Loop through all the values to create a list of all the data to add inside $data
            for ($i=0; $i < sizeof($values); $i++) {
                if ($data == "") $data = $values[$i];
                else $data .= ", " . $values[$i];
            }

            $stmt = $this->connection->prepare("INSERT INTO " . $table . " (" . $newKeys . ") VALUES (" . $newValues . ")"); //Prepare a statement to insert data into the database

            $array = explode(", ", $newValues); //Split the $newValues array by all the commas
            //Loop through all the values
            for($a=0; $a < sizeof($values); $a++) {
                $newData = explode(", ", $data); //Split the data by all the commas
                $stmt->bindParam($array[$a], $newData[$a]); //Bind the correct data to the correct parameter
            }

            $stmt->execute(); //Execute the prepared statement
            $this->log("Inserted data successfully into " . $table, true, false); //Logs an message which says the data is added successfully
        } catch (PDOException $e) {
            $this->log("Could not insert data into " . $table . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while inserting data
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Get data from the database
     * @param $table String -> The table where the data is stored
     * @param $withKey String -> The key associated with the value to check for data
     * @param $withValue String -> A value of the row you want to ask
     * @return boolean -> Is any data found?
     */
    public function exist($table, $withKey, $withValue) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not get any data. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return null; //Return the code to prevent it from doing other stuff
        }

        try {
            $stmt = $this->connection->prepare("SELECT * FROM `" . $table . "` WHERE `" . $withKey . "`='" . $withValue . "'"); //Prepare a statement to select data from the database
            $stmt->execute(); //Execute the prepared statement

            $this->log("Recieved data from database", true, false); //Logs a message which says the data was received
            return ($stmt->rowCount() > 0); //Returns a boolean who says if some data was found
        } catch (PDOException $e) {
            $this->log("Could not check for data in " . $table . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while asking the data
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Get data from the database
     * @param $table String -> The table where the data is stored
     * @param $withKey String -> The key associated with the value to get the data
     * @param $withValue String -> A value of the row you want to ask
     * @param $data String -> The data you want to ask (Example: username)
     * @return mixed -> Data received from the database
     */
    public function get($table, $withKey, $withValue, $data) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not get any data. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return null; //Return the code to prevent it from doing other stuff
        }

        try {
            $stmt = $this->connection->prepare("SELECT * FROM `" . $table . "` WHERE `" . $withKey . "`=:value"); //Prepare a statement to select data from the database
            $stmt->bindParam(":value", $withValue); //Bind the correct data to the parameters inside the prepared statement
            $stmt->execute(); //Execute the prepared statement

            //Check if the amount of rows is less than one.
            if ($stmt->rowCount() < 1) {
                $this->log("No data found for " . $withKey . " " . $withValue, true, true); //Logs an error which says there were zero results
                return null; //Return the code to prevent it from doing other stuff
            }

            //Loop through all the results
            while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                return $result[$data]; //Return the data from the database
            }

            $this->log("Recieved data from database", true, false); //Logs a message which says the data was received
        } catch (PDOException $e) {
            $this->log("Could not insert data into " . $table . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while asking the data
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Delete data from the database
     * @param $table String -> The table where the data is stored
     * @param $withKey String -> The key associated with the value to delete the row
     * @param $withValue String -> A value of the row that needs to be deleted
     */
    public function delete($table, $withKey, $withValue) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not delete data. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return; //Return the code to prevent it from doing other stuff
        }

        try {
            $stmt = $this->connection->prepare("DELETE FROM `" . $table . "` WHERE `" . $withKey . "`=:value"); //Create a prepared statement to delete the data from the database
            $stmt->bindParam(":value", $withValue); //Bind the correct data to the correct parameter in the prepared statement
            $stmt->execute(); //Execute the prepared statement

            $this->log("Deleted " . $withKey . " " . $withValue . " from " . $table . " successfully", true, false); //Logs a message which says the data was deleted successfully
        } catch (PDOException $e) {
            $this->log("Something went wrong while deleting " . $withKey . ":" . $withValue . " from " . $table, true, true); //Logs an error which says something went wrong while deleting the data
        }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Update data in database
     * @param $table String -> The table where the data is stored
     * @param $setKeys array -> An array with all the keys that need to be updated
     * @param $setValues array -> An array with all the new values
     * @param $withKey String -> The key associated with the value to update the row
     * @param $withValue String -> A value of the row that needs to be updated
     */
    public function update($table, $setKeys, $setValues, $withKey, $withValue) {
        //Check if there is an connection
        if ($this->connection == null) {
            $this->log("Could not update data. You need to open a connection first!", true, true); //Logs an error which says you need to open a connection first
            return; //Return the code to prevent it from doing other stuff
        }

        if (sizeof($setKeys) != sizeof($setValues)) {
            $this->log("Could not insert data. The number of setKeys and setValues ​​must be equal to each other", true, true); //Logs an error which says the number of setKeys and setValues needs to be equal to each other.
            return; //Return the code to prevent it from doing other stuff
        }

        try {

            $setData = ""; //Creates a new variable where a string of keys and values will be stored in

            //Loop though the keys
            for ($a=0; $a < sizeof($setKeys); $a++) {
                if ($setData == "") $setData = "`" . $setKeys[$a] . "`=:" . $setKeys[$a] . "";
                else $setData .= ", `" . $setKeys[$a] . "`=:" . $setKeys[$a] . "";
            }

            $stmt = $this->connection->prepare("UPDATE " . $table . " SET " . $setData . " WHERE `$withKey`='" . $withValue . "'"); //Prepare a statement to update the data

            //Loop through all the values
            for ($a=0; $a < sizeof($setValues); $a++) {
                $stmt->bindParam(":" . $setKeys[$a], $setValues[$a]); //Bind the correct value to the correct parameter inside the prepared statement
            }

            $stmt->execute(); //Execute the prepared statement
            $this->log($stmt->rowCount() . " records Updated successfully", true, false); //Logs a message which says that the records where updated successfully
        } catch (PDOException $e) {
            $this->log("Could not update data in " . $table . ": " . $e->getMessage(), true, true); //Logs an error which says something went wrong while updating data
        }
    }

    /**
     * @author Deepghost
     * @since 1.0.0
     * @description Update data in database
     * @param $table String -> The table where the data is stored
     * @param $setKeys array -> An array with all the keys that need to be updated
     * @param $setValues array -> An array with all the new values
     * @param $withKey String -> The key associated with the value to update the row
     * @param $withValue String -> A value of the row that needs to be updated
     */
    public function checkDB($askInput, $askTable, $askWhere, $askSpecs) {
      try {
      // $stmt = $this->conn()->prepare("SELECT $askInput FROM $askTable WHERE $askWhere = :askSpecs");
      $stmt = $this->connection->prepare("SELECT $askInput FROM $askTable WHERE $askWhere = :askSpecs");

      $stmt->execute(['askSpecs' => $askSpecs]);
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      return ($result);
     } catch (PDOException $e) {
        return $e->getMessage();
      }
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description To initiate a connection
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * @author Jesse de Vries
     * @since 1.0.0
     * @description Log messages into the log file and the console
     * @param $message String -> Message to log
     * @param $console Boolean -> If true the message will be send to the console
     * @param $error Boolean -> Is the log an error?
     */
    public function log($message, $console, $error) {
        if (file_exists('../../logs/logs.txt')) {
            $file = '../../logs/logs.txt'; //Select file to store logs in

            $current = file_get_contents($file); //Get content of file
            $current .= "[" . date("d/m/Y h:i:s") . "] >> " . $message . "\n"; //Create a message and add it to the content

            file_put_contents($file, $current); //Put message into the file
        }

        $color = ($error) ? '#f44242' : '#ffffff'; //Create the color of the log. If it is an error it will color red
        if ($console) echo "<script>console.log('%c  " . $message . "', 'background: " . $color  . "; color: #000000')</script>"; //If console is enabled the error will be shown in the console
    }

}
