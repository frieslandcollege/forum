<?php
$key = array_search(__FUNCTION__, array_column(debug_backtrace(), 'function'));
$file = debug_backtrace()[$key]['file'];

try {

    $fh = fopen($file, 'r');


    print_r(file($file));

    for ($i = 0; $i < sizeof(file($file)); $i++) {
        echo (file($file)[$i]);
    }

} catch (Exception $e) {
    $eh = new ErrorHandling($e->getMessage(), $e->getLine(), $file);
    $eh->execute();
}



class ErrorHandling {

  private $message, $line, $file;

  public function __construct($message, $line, $file) {
      $this->message = $message;
      $this->line = $line;

      $key = array_search(__FUNCTION__, array_column(debug_backtrace(), 'function'));
      $this->file = ($file == null) ? debug_backtrace()[$key]['file'] : $file;
  }

  public function setMessage() {
    return $this->message;
  }

  public function setLine() {
    return $this->line;
  }

  public function execute() {
      include "../../handling/Dependencies.php";
      $config = new Config();

     $json = json_encode(array("message"=>$this->message, "line"=>$this->line, "file"=>$this->file));

     $_SESSION['exception'] = $json;
     $_SESSION['HTTP_REFERER'] = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
     header("Location: " . $config->getBaseURL() . "exception");
  }

}
