<?php
/**
 * @author Deepghost
 * @since 1.0.0
 * @description The class which is responsible
 * @description for the functions concerning Registration
 */
class Register extends Connection
{

	// User Input

// public function __construct() {
// }

// Verification Flags
// Must all true in order for succesful registration
private $dirtyName = NULL, $dirtyMail = NULL, $dirtyPass  = NULL, $dirtyUser = NULL;
// ^^^^ Sets flags for the dirty user input which will be cleaned in further methods

private $cleanName = NULL, $cleanMail = NULL, $cleanPass  = NULL, $cleanUser = NULL;
// ^^^^ Sets flags for the cleaned user input for further processing

private $emptyFlag = NULL, $validFlag = NULL, $lengthFlag = NULL, $takenFlag = NULL;
// ^^^^ Sets flags for checking user related input

private $mailFlag  = NULL, $checkFlag = NULL;
// ^^^^ Sets flags for checking SQL related commands

private $shredder = NULL;
// ^^^^ Sets a flag if the shredder is activated

private $successFlag = NULL;


	public function shredRegVars()
	{

		$this->dirtyName = NULL; $this->dirtyMail = NULL; $this->dirtyPass  = NULL; $this->dirtyUser = NULL;
		$this->cleanName = NULL; $this->cleanMail = NULL; $this->cleanPass  = NULL; $this->cleanUser = NULL;
		$this->emptyFlag = NULL; $this->validFlag = NULL; $this->lengthFlag = NULL; $this->takenFlag = NULL;
		$this->mailFlag  = NULL; $this->checkFlag = NULL;
		$this->successFlag=NULL;
		$this->shredder  = TRUE;

		// echo "You done fucked boi";

	}



	public function prepRegVars($dirtyName, $dirtyMail, $dirtyPass, $dirtyUser)
	{
		// user_self_written_info
		$this->dirtyName = $dirtyName;
		$this->dirtyMail = $dirtyMail;
		$this->dirtyPass = $dirtyPass;
		$this->dirtyUser = $dirtyUser;
	}



	public function validateInputVars()
	{

		// Validate user input isn't empty

		if (!empty($this->dirtyName) || !empty($this->dirtyMail) || !empty($this->dirtyPass) || !empty($this->dirtyUser)) {
			$this->emptyFlag = true;
		}

		// Validate username input is legal

		if (preg_match("/^[a-zA-Z0-9.]+$/i", $this->dirtyUser) == true) {
			$this->validFlag = true;
		}
			if (strlen($this->dirtyUser) > 3)
			{
				$this->lengthFlag = true;
			} else {
				$this->lengthFlag = false;
			}
		}



	public function setValidInputVars()
	{
		if ($this->emptyFlag && $this->validFlag === true) {
			$this->cleanName = $this->dirtyName;
			$this->cleanUser = $this->dirtyUser;
			$this->cleanPass = $this->dirtyPass;
			$this->cleanMail = $this->dirtyMail;
			$this->dirtyName = null;
			$this->dirtyUser = null;
			$this->dirtyPass = null;
		}
		else {
			Register::shredRegVars();
		}
	}

	public function validateMailVars()
	{
		$this->cleanMail = filter_var($this->cleanMail, FILTER_VALIDATE_EMAIL);
		if ($this->cleanMail != $this->dirtyMail) {
			Register::shredRegVars();
		} elseif ($this->cleanMail == $this->dirtyMail) {

      $this->dirtyMail = null;
      $this->mailFlag = true;
    }
	}

	// Input Flags


	public function checkUserSQL() {
		$command = $this->checkDB(DB_USERNAME, DB_TABLE, DB_USERNAME,$this->cleanUser);
		if ($command[DB_USERNAME] == $this->cleanUser) {
			$this->takenFlag = false;
			Register::shredRegVars();
		} elseif ($command[DB_USERNAME] != $this->cleanUser) {
			$this->takenFlag = true;
		}
		$command2 = $this->checkDB(DB_MAIL, DB_TABLE, DB_MAIL,$this->cleanMail);
		if ($command2[DB_USERNAME] == $this->cleanMail) {
			$this->checkFlag = false;
			Register::shredRegVars();
		} elseif ($command2[DB_USERNAME] != $this->cleanMail) {
			$this->checkFlag = true;
		}
	}

public function flagsAway($dirtyName, $dirtyMail,$dirtyPass,$dirtyUser) {
	$this->connect(HOST, USERNAME, PASSWORD, DATABASE);
	$this->prepRegVars($dirtyName, $dirtyMail,$dirtyPass,$dirtyUser);
	$this->validateInputVars();
	$this->setValidInputVars();
	$this->validateMailVars();
	$this->checkUserSQL();
	$this->flagsResult();
}

public function flagsResult() {

	$flag = array(
	 $this->emptyFlag,
	 $this->validFlag,
	 $this->lengthFlag,
	 $this->takenFlag,
	 $this->mailFlag,
	 $this->checkFlag);

	foreach ($flag as $key => $val) {
		if ($val == 1) {
				return $this->prepSend();
		} elseif ($val != 1) {
				return Register::shredRegVars();
		}
	}
}

public function prepSend() {
	$this->insert("login", array("name", "mail", "password", "username", "role", "active"), array($this->cleanName, $this->cleanMail, $this->cleanPass, $this->cleanUser, "USER", "1"));
	$this->successFlag = true;
	header("Location: ".BASE_URL."?signup=success");
}
}
