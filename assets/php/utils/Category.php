<?php
/**
 * Project PhpStorm.
 * Created by jesse
 * On: 8-5-2019 20:53
 */

class Category {

    private $category;
    private $conn;
    private $key;

    public function __construct($category) {
        $this->conn = new Connection();
        $config = new Config();
        if ($category != null) {
            if (is_integer($category)) $this->key = "ID";
            else $this->key = "name";
            $this->category = $category;
        }
        $this->conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
    }

    public function getID() {
        return $this->conn->get("categories", $this->key, $this->category, "id");
    }

    public function getName() {
        return $this->conn->get("categories", $this->key, $this->category, "name");
    }

    public function getDate() {
        return $this->conn->get("categories", $this->key, $this->category, "created_at");
    }

}