<?php
class User {

    private $user;
    private $conn;
    private $key;

    public function __construct($user) {
        $this->conn = new Connection();
        $config = new Config();
        if ($user != null) {
            if (filter_var($user, FILTER_VALIDATE_EMAIL)) $this->key = "mail";
            else $this->key = "ID";
            $this->user = $user;
        }
        $this->conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());
    }

    public function getID() {
        return $this->conn->get("login", $this->key, $this->user, "ID");
    }

    public function getName() {
        return $this->conn->get("login", $this->key, $this->user, "name");
    }

    //https://iupac.org/wp-content/uploads/2018/05/default-avatar.png
    public function getIcon() {
        return $this->conn->get("login", $this->key, $this->user, "avatar");
    }

    public function getMail() {
        return $this->conn->get("login", $this->key, $this->user, "mail");
    }

    public function getPassword() {
        return $this->conn->get("login", $this->key, $this->user, "password");
    }

    public function getUsername() {
        return $this->conn->get("login", $this->key, $this->user, "username");
    }

    public function getRole() {
        return $this->conn->get("login", $this->key, $this->user, "role");
    }

    public function isActive() {
        return $this->conn->get("login", $this->key, $this->user, "active");
    }

    public function isAdmin() {
        return $this->conn->get("login", $this->key, $this->user, "admin");
    }

    public function getFunction() {
        return $this->conn->get("login", $this->key, $this->user, "function");
    }

    public function getAbout() {
        return $this->conn->get("login", $this->key, $this->user, "about");
    }

    public function getColor() {
        return $this->conn->get("login", $this->key, $this->user, "color");
    }

}
