<?php
session_start();
include "handling/Dependencies.php";
$config = new Config();

$_SESSION['loggedIn'] = false;

/**
* @author Deepghost
* @since 1.0.0
* @description Prepares user input for cleaning
*/
//$regs = new Register;

if (sizeof($_POST) === 4) {
//    $regs->flagsAway($_POST['name'], $_POST['mail'], $_POST['password'], $_POST['usrname']);
     $conn = new Connection();
     $conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

     if (!$conn->exist("login", "username", $_POST['usrname']) && !$conn->exist("login", "mail", $_POST['mail'])) {
         $conn->insert("login", array("name", "avatar", "mail", "password", "username", "role", "active"), array($_POST['name'], "https://iupac.org/wp-content/uploads/2018/05/default-avatar.png", $_POST['mail'], $_POST['password'], $_POST['usrname'], "USER", 1));

         $_SESSION['registerError'] = "Account is succesvol aangemaakt. Je kunt nu inloggen!";
         header("Location: " .   $_SERVER['HTTP_REFERER']);
     } else {
         $_SESSION['registerError'] = "Er bestaat al een account met deze gebruikersnaam en/of emailadres";
         header("Location: " . $_SERVER['HTTP_REFERER']);
     }
 } else {
     $_SESSION['registerError'] = "Je moet alle velden invullen!";
     header("Location: " . $_SERVER['HTTP_REFERER']);
}
