<?php
session_start();
include "handling/Dependencies.php";
$_SESSION['loggedIn'] = false;

$conn = new Connection();
$config = new Config();

$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

if (sizeof($_POST) === 2) {

    $username = $_POST['usrname'];
    $password = $_POST['password'];

    if($conn->exist("login", "username", $username) && $conn->exist("login", "password", $password)) {
        $_SESSION['loggedIn'] = true;
        $_SESSION['userId'] = $conn->get("login", "username", $username, "ID");
        header("Location: " . $config->getBaseURL() . "content/public");
    } else {
        $_SESSION['loginError'] = "The combination of the username and password is invalid!";
        header("Location: " .   $_SERVER['HTTP_REFERER']);
    }

} else {
    $_SESSION['loginError'] = "Invalid POST!";
    header("Location: " . $_SERVER['HTTP_REFERER']);
}