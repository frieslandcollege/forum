<?php
spl_autoload_register( 'autoload');
$url = getDir();

function autoload( $class, $dir = null ) {
    GLOBAL $url;

    if (is_null($dir))
        $dir = $url;

    foreach (scandir($dir) as $file) {

        //Directory?
        if (is_dir($dir . $file) && substr($file, 0, 1) !== '.')
            autoload($class, $dir . $file . '/');

        // php file?
        if (substr($file, 0, 2) !== '._' && preg_match("/.php$/i", $file)) {

            // filename matches class?
            if (str_replace('.php', '', $file) == $class || str_replace('.class.php', '', $file) == $class) {
                include $dir . $file;
            }
        }
    }
}

$c = new Content(null);








function getDir() {
    $baseUrl = 'C:\xampp\htdocs\School\Forum';
    $url = getcwd();

    if ($baseUrl === $url) {
        return "../";
    }

    $url = str_replace($baseUrl, "", $url);
    $array = explode("\\", $url);

    $path = "";
    for ($i = 0; $i < sizeof($array); $i++) {
        if ($array[$i] != null) $path .= "../";
    }

    return $path;
}

