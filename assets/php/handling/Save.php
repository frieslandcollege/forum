<?php
if (!(isset($_POST['CATEGORY']) && isset($_POST['TITLE']) && isset($_POST['BODY']) && isset($_GET['AUTHOR']))) die("Invalid post");

include "Dependencies.php";
$connection = new Connection();
$config = new Config();
$connection->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

if (isset($_POST['CUSTOM_CATEGORY']) && !$connection->exist("categories", "name", $_POST['CUSTOM_CATEGORY'])) {
    $connection->insert("categories", array("name", "created_at"), array($_POST['CUSTOM_CATEGORY'], date("Y-m-d H:i:s")));
    $_POST['CATEGORY'] = $_POST['CUSTOM_CATEGORY'];
}
$category = new Category($_POST['CATEGORY']);

$keys = array("category_id", "title", "body", "author", "created_at");
//Encoding body to stop SQL from fucking with my characters
$values = array($category->getID(), $_POST['TITLE'], base64_encode($_POST['BODY']), $_GET['AUTHOR'], date("Y-m-d H:i:s"));
$connection->insert("posts", $keys, $values);
echo "<script>window.location.href='" . $config->getBaseURL() . "content/public/posts.php" . "'</script>";