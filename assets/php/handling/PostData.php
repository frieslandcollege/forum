<?php
include "Dependencies.php";
$conn = new Connection();
$config = new Config();
$conn->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

$stmt = $conn->getConnection()->prepare("SELECT * FROM `posts`");
$stmt->execute();

if ($stmt->rowCount() < 1) {
    die("NO POSTS!");
}
$array = array();

while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $post = new Posts($result['id']);
    $author = new User($post->getAuthor());
    $postData = array();

    $postData['id'] = $post->getID();
    $postData['category'] = $post->getCategory();
    $postData['title'] = $post->getTitle();
    $postData['body'] = $post->getBody();
    $postData['author']['name'] = $author->getID();
    $postData['author']['id'] = $author->getName();
    $postData['created_at'] = $post->getDate();
    $postData['up_votes'] = $post->getUpvotes();
    $postData['down_votes'] = $post->getDownvotes();
    array_push($array, $postData);
}

echo json_encode($array);