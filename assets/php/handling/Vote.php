<?php
include "Dependencies.php";
if (!isset($_POST['vote']) || !isset($_POST['user']) || !isset($_POST['post'])) return;

$vote = strtoupper($_POST['vote']);

if ($vote == "UP") {
    $post = new Posts($_POST['post']);
    $user = new User($_POST['user']);
    upVote($post, $user);

    print_r($_POST);

} else if ($vote == "DOWN") {
    $post = new Posts($_POST['post']);
    $user = new User($_POST['user']);
    downVote($post, $user);

    print_r($_POST);
}



function upVote($post, $user) {
    $upVotes = $post->getUpvotes();
    $downVotes = $post->getDownvotes();
    $upArray = explode(", ", $upVotes);
    $downArray = explode(", ", $downVotes);
    if (in_array($user->getID(), $upArray)) {
        if (($key = array_search($user->getID(), $upArray)) !== false) unset($upArray[$key]);

        $upString = "";
        for ($i = 0; $i < sizeof($upArray); $i++) {
            if ($i == 1) $upString = $upArray[$i];
            else $upString .= ", " . $upArray[$i];
        }

        $post->setUpvotes($upString);
        return;
    }

    if (($key = array_search($user->getID(), $downArray)) !== false) unset($downArray[$key]);
    array_push($upArray, $user->getID());

    $upString = "";
    for ($i = 0; $i < sizeof($upArray); $i++) {
        if ($i == 0) $upString = $upArray[$i];
        else $upString .= ", " . $upArray[$i];
    }

    $downString = "";
    for ($i = 0; $i < sizeof($downArray); $i++) {
        if ($i == 0) $downString = $downArray[$i];
        else $downString .= ", " . $downArray[$i];
    }

    $post->setUpvotes($upString);
    $post->setDownVotes($downString);
}

function downVote($post, $user) {
    $upVotes = $post->getUpvotes();
    $downVotes = $post->getDownvotes();
    $upArray = explode(", ", $upVotes);
    $downArray = explode(", ", $downVotes);
    if (in_array($user->getID(), $downArray)) {
        if (($key = array_search($user->getID(), $downArray)) !== false) unset($downArray[$key]);

        $downString = "";
        for ($i = 0; $i < sizeof($downArray); $i++) {
            $downString .= ", " . $downArray[$i];
        }

        $post->setDownVotes($downString);
        return;
    }

    if (($key = array_search($user->getID(), $upArray)) !== false) unset($upArray[$key]);
    array_push($downArray, $user->getID());

    $upString = "";
    for ($i = 0; $i < sizeof($upArray); $i++) {
        if ($i == 0) $upString = $upArray[$i];
        else $upString .= ", " . $upArray[$i];
    }

    $downString = "";
    for ($i = 0; $i < sizeof($downArray); $i++) {
        if ($i == 0) $downString = $downArray[$i];
        else $downString .= ", " . $downArray[$i];
    }

    $post->setUpvotes($upString);
    $post->setDownVotes($downString);
}