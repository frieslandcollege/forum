<?php
if (!isset($_GET['ID'])) die("Invalid post");
include "Dependencies.php";

$connection = new Connection();
$config = new Config();
$connection->connect($config->getHost(), $config->getUsername(), $config->getPassword(), $config->getDatabase());

$connection->delete("posts", "id", $_GET['ID']);
echo "<script>window.location.href='" . $config->getBaseURL() . "content/public/posts.php" . "'</script>";